module Spree
  class WebpayController < StoreController
    skip_before_action :verify_authenticity_token

    before_action :load_data

    def confirmation
      if @payment.blank?
        return failure
      end

      provider = @payment_method.provider.new

      # TODO: Fix this once tbk response on cancellation matches new standard
      token = params['token_ws'] ? params['token_ws'] : params['TBK_TOKEN']

      response = provider.commit token
      puts response

      return success
    end

    # GET/POST spree/webpay/success
    def success
      # To clean the Cart
      session[:order_id] = nil
      @current_order     = nil

      # Reload payment and order to update payment details
      @payment.reload
      @order.reload

      redirect_to root_path and return if @payment.blank?

      if @payment.failed? || !@payment.accepted
        # reviso si el pago esta fallido y lo envio a la vista correcta
        puts @payment.failed?
        puts @payment.accepted
        redirect_to webpay_failure_path(params.to_enum.to_h) and return
      else
        if @order.completed? || @payment.accepted
          puts "success 1"
          flash.notice = Spree.t(:order_processed_successfully)
          flash['order_completed'] = true
          redirect_to completion_route and return
        else
          puts "failed 2"
          redirect_to webpay_failure_path(params.to_enum.to_h) and return
        end
      end
    end

    # GET spree/webpay/failure
    def failure
      @order
    end

    private
    # Carga los datos necesarios
    def load_data
      @payment = Spree::Payment.find_by(webpay_tx_id: params['session_id'])

      # Verifico que se encontro el payment
      # redirect_to webpay_failure_path(params) and return unless @payment
      unless @payment.blank?
        @payment_method = @payment.payment_method
        @order          = @payment.order
      end
    end

    # Same as CheckoutController#completion_route
    def completion_route
      spree.order_path(@order)
    end
  end
end
