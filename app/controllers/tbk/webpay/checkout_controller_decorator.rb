module Tbk::Webpay::CheckoutControllerDecorator
  def edit
    @payment = @order.payments.order(:id).last
    webpay_state = Spree::Gateway::WebpayPlus.STATE

    if params[:state] == webpay_state || @order.state == webpay_state
      payment_method     = @order.webpay_payment_method
      tx_id              = @payment.webpay_tx_id
      amount             = @order.webpay_amount
      return_url         = webpay_return_url+'?session_id='+tx_id
      provider = payment_method.provider.new
      response = provider.pay(@order.number, tx_id, amount, return_url)

      @tbk_url = response.url
      @tbk_token = response.token

      respond_to do |format|
        format.html { render text: response }
      end
    end
  end
end

::Spree::CheckoutController.prepend(Tbk::Webpay::CheckoutControllerDecorator)

