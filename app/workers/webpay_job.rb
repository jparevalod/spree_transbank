class WebpayJob < ActiveJob::Base
  queue_as :webpay

  retry_on StandardError, attempts: 5 do |job, error|
    message = "Stopped retrying #{job.class} (JID #{job.job_id})
               with #{job.arguments.join(', ')} due to
               '#{error.class} - #{error.message}'.
               This job was retried for #{job.executions} times.".squish

    Rails.logger.error(message)
  end

  def perform(payment)
    Spree::UpdateWebpayPaymentService.call(payment)
  end
end

