require 'yaml'

module Tbk
  module Webpay
    class Config
      attr_accessor :config_filepath, :commerce_code, :api_key, :integration_type

      # Public: Loads the configuration file tbk-webpay.yml
      # If it's a rails application it will take the file from the config/ directory
      #
      # env - Environment.
      #
      # Returns a Config object.
      def initialize env = nil, config_override = nil
        if env
          # For non-rails apps
          @config_filepath = File.join(File.dirname(__FILE__), "..", "..", "config", "tbk.yml")
          load(env)
        else
          @config_filepath = File.join(Rails.root, "config", "tbk.yml")
          load(Rails.env)
        end
      end

      private

      # Private: Initialize variables
      #
      # rails_env - Environment.
      #
      # Returns nothing.
      def load(rails_env)
        config = YAML.load_file(@config_filepath)[rails_env]
        @commerce_code = config['commerce_code']
        @api_key = config['api_key']
        @integration_type = config['integration_type']
      end
    end
  end
end
