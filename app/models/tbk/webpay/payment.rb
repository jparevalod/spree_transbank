require 'ostruct'

module Tbk
  module Webpay
    class Payment
      # Public: Loads the configuration file tbk-webpay.yml
      # If it's a rails application it will take the file from the config/ directory
      #
      # env - Environment.
      #
      # Returns a Config object.
      def initialize env = nil
        config ||= Tbk::Webpay::Config.new(env)
        Transbank::Webpay::WebpayPlus::Base.commerce_code = config.commerce_code
        Transbank::Webpay::WebpayPlus::Base.api_key = config.api_key
        Transbank::Webpay::WebpayPlus::Base.integration_type = config.integration_type
      end

      # Public: Initial communication from the application to Webpay servers
      #
      # tbk_total_price - integer - Total amount of the purchase. Last two digits are considered decimals.
      # tbk_order_id - integer - The purchase order id.
      # session_id - integer - The user session id.
      #
      # Returns a REST response to be rendered by the application
      def pay(buy_order, session_id, amount, return_url)
        tbk_params = {
          'buy_order' => buy_order,
          'session_id' => session_id,
          'amount' => amount,
          'return_url' => return_url
        }

        tbk_string_params = ""

        tbk_params.each do |key, value|
          tbk_string_params += "#{key}=#{value}&"
        end

        Rails.logger.info '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
        Rails.logger.info 'Request: Transaction Create'
        Rails.logger.info tbk_string_params
        Rails.logger.info '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'

        response = Transbank::Webpay::WebpayPlus::Transaction::create(tbk_params.to_options)

        Rails.logger.info '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
        Rails.logger.info 'Response: Transaction Create'
        Rails.logger.info 'url:' + response.url
        Rails.logger.info 'token:' + response.token
        Rails.logger.info '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'

        return response
      end

      # Public: Confirmation callback executed from Webpay servers.
      # Checks Webpay transactions workflow.
      #
      # Returns a string redered as text.
      def commit(token)
        tbk_response_codes = {
          'SUCCESS' => 0,
          'INPUT_FAILURE' => -1,
          'CARD_FAILURE' => -2,
          'TBK_FAILURE' => -3,
          'OWNER_FAILURE' => -4,
          'FRAUD_FAILURE' => -5
        }

        begin
          response = Transbank::Webpay::WebpayPlus::Transaction::commit(token: token)
          @payment = Spree::Payment.find_by(webpay_tx_id: response.session_id)
          @payment.reload
          @verbose = @payment.payment_method.preferred_verbose
          @order = Spree::Order.find_by number: response.buy_order
          @order.reload
        rescue Transbank::Webpay::WebpayPlus::Errors::TransactionCommitError
          # This handles the User Cancellation Flow
          response = OpenStruct.new({:response_code => tbk_response_codes['OWNER_FAILURE']})
        end

        @logfile = "#{Time.now.to_date.to_s.underscore}_webpay"
        begin
          MultiLogger.add_logger("#{@logfile}") if @verbose
        rescue
          # Nothing for now
        end

        if response.response_code == tbk_response_codes['SUCCESS']

          logger("Inicio", "") if @verbose

          accepted = true

          # the confirmation is invalid if order_id is unknown
          if not order_exists?
            accepted = false
          end

          # double payment
          if order_paid?
            accepted = false
          end

          # wrong amount
          if not order_right_amount? response.amount
            accepted = false
          end

          if accepted && !['failed', 'invalid'].include?(@payment.state)
            logger("Valid", ":)") if @verbose

            response_hash = response.instance_values
            # Store token in webpay params just in case it's needed in the future
            response_hash['token'] = token

            @payment.update_column(:accepted, true)
            @payment.update_attributes webpay_params: response_hash

            if @payment.payment_method.preferred_use_async
              # TODO: IMPLEMENT THIS
              WebPayJob.perform_later(@payment.id, "accepted")
            else
              @payment.capture! @payment.amount
              @payment.order.next!
            end

            logger("Completed", ":)") if @verbose
            return "ACEPTADO"
          else
            logger("Invalid", ":(") if @verbose
            unless ['completed', 'failed', 'invalid'].include?(@payment.state)
              @payment.update_column(:accepted, false)

              if @payment.payment_method.preferred_use_async
                # TODO: IMPLEMENT THIS
                WebPayJob.perform_later(@payment.id, "rejected")
              else
                @payment.started_processing!
                @payment.failure!
              end

            end
            logger("Rejected", ":(") if @verbose
            return "RECHAZADO"
          end

        else  # TBK_RESPUESTA != 0 (Failure)
          logger("TBK_RESPUESTA != 0", response.response_code) if @verbose
          return "RECHAZADO"
        end

      end

      private

      # Private: Checks if an order exists and is ready for payment.
      #
      # order_id - integer - The purchase order id.
      #
      # Returns a boolean indicating if the order exists and is ready for payment.
      def order_exists?
        result = @order.is_a? Spree::Order
        logger(__method__.to_s, result) if @verbose
        return result
      end

      # Private: Checks if an order is already paid.
      #
      # order_id - integer - The purchase order id.
      #
      # Returns a boolean indicating if the order is already paid.
      def order_paid?
        return false unless @order
        result = @order.paid? || @order.payments.completed.any?
        logger(__method__.to_s, result) if @verbose
        return result
      end

      # Private: Checks if an order has the same amount given by Webpay.
      #
      # order_id - integer - The purchase order id.
      # tbk_total_amount - The total amount of the purchase order given by Webpay.
      #
      # Returns a boolean indicating if the order has the same total amount given by Webpay.
      def order_right_amount? tbk_total_amount
        return false unless @order
        result = @order.webpay_amount.to_i == tbk_total_amount.to_i
        logger(__method__.to_s, result) if @verbose
        return result
      end

      def logger message, value
        Rails.logger.send("#{@logfile}").info("[#{@order.number} #{@order.try(:state)}] #{message} #{value}")
      end
    end
  end
end
