module Tbk::Webpay::PaymentDecorator
  def self.prepended(base)
    base.scope :from_webpay, -> { joins(:payment_method).where(spree_payment_methods: {type: Spree::Gateway::WebpayPlus.to_s}) }
    base.serialize(:webpay_params, Hash)
    base.after_initialize :set_webpay_tx_id
  end

  def webpay?
    self.payment_method.type == "Spree::Gateway::WebpayPlus"
  end

  def webpay_card_number
    "XXXX XXXX XXXX #{webpay_params['card_detail']['card_number']}"
  end

  def webpay_quota_type
    case webpay_params['payment_type_code']
    when "VP"
      return "Prepago"
    when "VN"
      return "Sin Cuotas"
    when "VC"
      return "Cuotas"
    when "SI"
      return "3 Cuotas Sin Interés"
    when "S2"
      return "2 Cuotas Sin Interés"
    when "NC"
      return "N Cuotas Sin Interés"
    when "VD"
      return "Débito"
    else
      return webpay_params['payment_type_code']
    end
  end

  def webpay_payment_type
    if webpay_params['payment_type_code'] == 'VD'
      return "Redcompra"
    else
      return "Crédito"
    end
  end

  private
  # Private: Setea un tx_id unico.
  #
  # Returns Token.
  def set_webpay_tx_id
    self.webpay_tx_id ||= generate_webpay_tx_id
  end

  # Private: Genera el tx_id unico.
  #
  # Returns generated tx_id.
  def generate_webpay_tx_id
    Digest::MD5.hexdigest("#{order.number}#{order.payments.count}") if order
  end
end


::Spree::Payment.prepend(Tbk::Webpay::PaymentDecorator)
