module Tbk::Webpay::OrderDecorator
  # Se re-define cuales son pagos pendientes
  def pending_payments
    payments.select{ |payment| payment.checkout? or payment.completed? }
  end

  # Step only visible when payment failure

  def self.prepended(base)
    base.checkout_flow do
      go_to_state :address
      go_to_state :delivery
      go_to_state :payment, if: ->(order) { order.payment? || order.payment_required? }
      go_to_state :webpay,  if: ->(order) { order.has_webpay_payment_method? || order.state == Spree::Gateway::WebpayPlus.STATE }
      go_to_state :confirm, if: ->(order) { order.confirmation_required? }
      go_to_state :complete
      remove_transition from: :delivery, to: :confirm, unless: ->(order) { order.confirmation_required? }
    end
  end

  # Indica si la orden tiene algun pago con Webpay completado con exito
  #
  # Return TrueClass||FalseClass instance
  def webpay_payment_completed?
    if payments.completed.from_webpay.any?
      true
    else
      false
    end
  end

  def webpay_client_name
    if ship_address
      ship_address.full_name
    else
      "#{user.firstname} #{user.lastname}"
    end
  end


  # Indica si la orden tiene asociado un pago por Webpay
  #
  # Return TrueClass||FalseClass instance
  def has_webpay_payment_method?
    payments.valid.from_webpay.any?
  end

  # Devuelvela forma de pago asociada a la order, se extrae desde el ultimo payment
  #
  # Return Spree::PaymentMethod||NilClass instance
  def webpay_payment_method
    has_webpay_payment_method? ? payments.valid.from_webpay.order(:id).last.payment_method : nil
  end

  # Entrega en valor total en un formato compatible con el estandar de Webpay
  #
  # Return String instance
  def webpay_amount
    # TODO - Ver que pasa cuando hay decimales
    (total.to_i)
  end

  # ToDo - Esto deberia ser un helper
  # Public: Entrega el webpay_tx_id del pago.
  #
  # Returns webpay_tx_id.
  def dummy_webpay_tx_id
    Digest::MD5.hexdigest("#{self.number}#{self.payments.count}")
  end
end

::Spree::Order.prepend(Tbk::Webpay::OrderDecorator)
