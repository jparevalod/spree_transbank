module Spree
  class UpdateWebpayPaymentService
    def call(payment)
      order = payment.order

      payment.complete

      unless order.state == Spree::Gateway::WebpayPlus.STATE
        order.update_column(:state, Spree::Gateway::WebpayPlus.STATE)
      end

      order.next! unless order.complete?

      payment
    end

    def self.call(*args)
      new.call(*args)
    end
  end
end
