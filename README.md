# Spree Transbank Gem

Spree Gem for Transbank Payment Methods

Based remotely on spree_tbk_webpay_ws gem [(1)] by AcidLabs

### Payment Methods:
- Webpay Plus
- Patpass (WIP)


### Installation
------------

Add spree_transbank gem to your Gemfile:

```ruby
gem "spree_transbank", git: 'https://bitbucket.org/jparevalod/spree_transbank.git'
```

Install dependencies:

```shell
bundle
```

Create a file named tbk.yml inside your project's config/ folder with the following info (these are test values):

```
production:
  commerce_code: "YOUR_COMMERCE_CODE"
  api_key: "YOUR_API_KEY"
  integration_type: "LIVE"
staging:
  commerce_code: "597055555532"
  api_key: "579B532A7440BB0C9079DED94D31EA1615BACEB56610332264630D42D0A36B1C"
  integration_type: "TEST"
development:
  commerce_code: "597055555532"
  api_key: "579B532A7440BB0C9079DED94D31EA1615BACEB56610332264630D42D0A36B1C"
  integration_type: "TEST"
```

Execute generator:

```shell
bundle exec rails g spree_transbank:install
```

Run migrations:

```shell
bundle exec rake db:migrate
```

Add transbank-sdk Gem to your Gemfile

```ruby
# Transbank
gem 'transbank-sdk'

```

Run Bundle Install:

```shell
bundle install
```

**OPTIONAL FOR A FRIENDLIER CHECKOUT**: Modify your app/views/spree/checkout/edit.html.erb with the following:

```
<% if @order.state == Spree::Gateway::WebpayPlus.STATE %>
  <% unless @order.confirm? %>
    <div id="checkout-summary" class="col-md-12 col-lg-auto" data-hook="checkout_summary_box">
      <div class="checkout-summary-container position-sticky">
        <%= render partial: 'summary', locals: { order: @order } %>
        <div data-hook="buttons">
          <%= link_to Spree.t(:webpay_checkout), @tbk_url+'?token_ws='+@tbk_token, :class => 'btn btn-primary text-uppercase font-weight-bold w-100 checkout-content-save-continue-button', :style => 'padding:20px' %>
        </div>
      </div>
    </div>
  <% end %>
<% else %>
  <% unless @order.confirm? %>
    <div id="checkout-summary" class="col-md-12 col-lg-auto" data-hook="checkout_summary_box">
      <div class="checkout-summary-container position-sticky">
        <%= render partial: 'summary', locals: { order: @order } %>
        <div data-hook="buttons">
          <% submit_label_key = @order.confirm? ? :place_order : :save_and_continue %>
          <%= submit_tag Spree.t(submit_label_key), class: 'btn btn-primary text-uppercase font-weight-bold w-100 checkout-content-save-continue-button' %>
        </div>
      </div>
    </div>
  <% end %>
<% end %>
```

... and override app/views/spree/checkout/_webpay.html.erb with the following:

```
<div class="panel panel-default" id="payment" data-hook>
  <div class="panel-heading">
    <h3 class="panel-title"><%= Spree.t(:payment_information) %></h3>
  </div>
  <br>
  <div class="panel-body" data-hook="checkout_payment_step">
    <h4>
      <b><%= Spree.t(:payment_type) %>: </b> <%= Spree.t(:webpay_plus) %>
      <%= checkout_edit_link('payment') %>
    </h4>
  </div>
</div>
```

### Useful Card Data for Webpay Plus Testing
-------

Card:

- Crédito Visa (aprobado):

    - Nº: 4051885600446623
    - Año Expiración: any
    - Mes Expiración: any
    - CVV: 123

- Crédito Mastercard (rechazado):

    - Nº: 5186059559590568
    - Año Expiración: any
    - Mes Expiración: any
    - CVV: 123

-  Tarjeta de débito (aprobado o rechazado):
    - Nº 12345678


When inside bank provider you can use RUT **11.111.111-1** and password **123**

[(1)]: https://gitlab.k8s.acidlabs.io/spree-extensions/spree_tbk_webpay_ws/
