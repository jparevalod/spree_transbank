class AddAcceptedToSpreePayments < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_payments, :accepted, :boolean
  end
end
