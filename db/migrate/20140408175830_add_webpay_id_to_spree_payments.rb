class AddWebpayIdToSpreePayments < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_payments, :webpay_tx_id, :string
  end
end
