class ChangeWebpayAttributesToSpreePayments < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_payments, :webpay_params, :text
  end
end
