module SpreeTransbankConfig
  CONFIG = YAML.load(ERB.new(File.read(Rails.root.join("config/tbk.yml"))).result)[Rails.env]

  COMMERCE_CODE = CONFIG['commerce_code']
  API_KEY = CONFIG['api_key']
  INTEGRATION_TYPE = CONFIG['integration_type']

end
