# encoding: UTF-8
lib = File.expand_path('../lib/', __FILE__)
$LOAD_PATH.unshift lib unless $LOAD_PATH.include?(lib)

require 'spree_transbank/version'

Gem::Specification.new do |s|
  s.name    = "spree_transbank"
  s.platform    = Gem::Platform::RUBY
  s.version     = SpreeTransbank.version
  s.required_ruby_version = '>= 2.7.0'

  s.summary     = "Integrates WebpayPlus and PatPass payment methods into Spree"
  s.description = "Integrates WebpayPlus and PatPass payment methods into Spree"

  s.authors  = ["Juan Pablo Arévalo"]
  s.email    = ["juanpablo@3a.cl"]
  s.homepage = "http://www.3a.cl"
  s.date     = "2021-01-15"

  s.files       = `git ls-files`.split("\n").reject { |f| f.match(/^spec/) && !f.match(/^spec\/fixtures/) }
  s.require_path = 'lib'
  s.requirements << 'none'

  spree_version = '>= 4.1.0', '< 5.0'
  s.add_dependency 'spree_core', spree_version
  s.add_dependency 'spree_backend', spree_version
  s.add_dependency 'spree_extension'

  s.add_dependency 'multi_logger'
  s.add_dependency 'transbank-sdk'

  s.add_development_dependency 'appraisal'
  s.add_development_dependency 'annotate'
  s.add_development_dependency 'awesome_print'
  s.add_development_dependency 'better_errors'
  s.add_development_dependency 'binding_of_caller'
  s.add_development_dependency 'capybara'
  s.add_development_dependency 'capybara-screenshot'
  s.add_development_dependency 'coffee-rails'
  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'factory_bot'
  s.add_development_dependency 'ffaker'
  s.add_development_dependency 'mysql2'
  s.add_development_dependency 'pg'
  s.add_development_dependency 'pry-rails'
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'sass-rails'
  s.add_development_dependency 'simplecov'
  s.add_development_dependency 'sqlite3'
  s.add_development_dependency 'webdrivers'
end
